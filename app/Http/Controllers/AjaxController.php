<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Card;

class AjaxController extends Controller
{
    //

    public function index(Request $request){

        $action = $request->input("action");
        return $this->$action($request);
        
    }

    public function error($msg){

        $data['success'] = "no";
        $data['message'] = $msg;
        return $data;
    }

    public function success($msg, $card){

        $data['success'] = "yes";
        $data['message'] = $msg;
        $data['card'] = $card;
        return $data;
    }


    private function addTodo($request){
        $card = new Card;
        $title = trim($request->input("title"));

        $oldCard = $card->where('title', $title)->first();

       if(!$oldCard){
        $card->title = $title;
        $card->type = "todo";           
        $card->save();
        return $this->success("Card added",$card);
       }       

        return $this->error("Card already Exists");
    }


    private function ChangeCardType($request){

        $card = Card::find($request->id);
        $newType =  $request->type;


       if($card){       
        $card->type = $newType;           
        $card->save();
        return $this->success("Card Updated",$card);
       }       

        return $this->error("Something whent wrong.");
    }

    private function SortCardList($request){
        $lis = $request->input("lis");

        if(is_array($lis)){
            foreach($lis as $card_order => $card_id){
                $card = Card::find($card_id);
                if($card){       
                $card->card_order = $card_order;           
                $card->save();        
               }   

            }
        }
    
       return $this->success("Card Updated",$lis);

    }

    
    
}
