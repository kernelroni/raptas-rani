<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Card;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();

        $todoCollection = Card::where("type" , "todo")->orderBy("card_order")->get();
        $inworkCollection = Card::where("type" , "in-work")->orderBy("card_order")->get();
        $doneCollection = Card::where("type" , "done")->orderBy("card_order")->get();

        $data['todos'] = $todoCollection;
        $data['inworks'] = $inworkCollection;
        $data['done'] = $doneCollection;
        return view('card', $data);
    }

}
