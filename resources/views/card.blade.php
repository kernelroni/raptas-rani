@extends('layouts.app')

@section('content')
<div class="container raptacards">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card text-white bg-info mb-3">
                <h5 class="card-header">{{ __('Todo List') }}</h5>

                <div class="card-body">
                    <ul class="list-group list-group-flush" id="card-todo">

                        @foreach ($todos as $todo)
                        <li class="list-group-item"
                            data-id="{{$todo->id}}"
                        > <i class="fas fa-user"></i> {{$todo->title}} </li>
                        @endforeach

                    </ul>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary" id="add-todo-btn" data-toggle="modal" data-target="#ModalTodoItem"> Add Todo </button>
                </div>

            </div>
        </div>
        <div class="col-md-4">
            <div class="card text-white bg-danger mb-3">
                <h5 class="card-header">{{ __('In-Work') }}</h5>

                <div class="card-body" >
                    <ul class="list-group list-group-flush" id="card-in-work">
                            @foreach ($inworks as $todo)
                            <li class="list-group-item"
                                data-id="{{$todo->id}}"
                            ><i class="fas fa-user"></i> {{$todo->title}}</li>
                            @endforeach                        

                    </ul>
                </div>
                <div class="card-footer">
                   
                </div>                
            </div>
        </div>
        <div class="col-md-4">
            <div class="card text-white bg-success mb-3">
                <h5 class="card-header">{{ __('Done') }}</h5>

                <div class="card-body" >
                    <ul class="list-group list-group-flush" id="card-done">
                            @foreach ($done as $todo)
                            <li class="list-group-item"
                                data-id="{{$todo->id}}"
                            ><i class="fas fa-user"></i> {{$todo->title}}</li>
                            @endforeach 
                      </ul>
                </div>
                <div class="card-footer">
                    
                </div>
            </div>
        </div>                
    </div>
</div>

<div class="modal" id="ModalTodoItem" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Todo Item</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <div class="alert alert-danger" id="error-alert" role="alert">
                        
                </div>
                <div class="alert alert-success" id="success-alert" role="alert">
                        
                </div>
          <p><input type="text" id="todo-input" class="form-control mb-3" placeholder="Todo"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="modal-save-btn">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection
