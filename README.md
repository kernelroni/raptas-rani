# Raptas-Rani

#instalation procedure.

## First Install fresh Laravel with Auth/Ui package. or you can do however you want or by composer update.

## You can download the entire project from here and just change the db settings in env file.
```
http://returnperfect.com/raptasoft.zip
```

## commands to install the app.
```
composer create-project --prefer-dist laravel/laravel raptasoftnew 

```

```
composer require laravel/ui --dev
```

```
php artisan ui vue --auth
```

```
php artisan migrate
```


## Change the database/host etc from the .env file.


## Import this sql to your database.
``` 
raptadb/cards.sql 
```

# Final Steps

### replace your new laravel app/Http with this repo app/Http folder. 

### replace your new laravel app/Http with this repo app/Http folder. 

### replace resources/views with this repo resources/views folder.

### add app/Card.php model from this repo to your app/ folder.

### replace reutes/web.php with this repo reutes/web.php file.


#Note

```

If you have some issues with installation 
please feel free to contact me, We can fix it together. 
Contact me 
kernelroni@gmail.com
Phone : 01757304470
skype : kernelroni

```
