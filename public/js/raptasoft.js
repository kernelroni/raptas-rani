var raptas = {

	ajax :  function(formData, callback){

		jQuery.ajax({
			url: "/ajax",
			type: "POST",
			data: formData,
			dataType: "json",
			success:function(res){
				callback(res);
			}
		  });

	},

	AddTodo : function(){
		
		var data = {};
		var title = jQuery("#todo-input").val();
		console.log(title);
		data.title = title;
		data.action = "addTodo";

		this.ajax(data,function(res){

			if(res.success == "yes"){
				// add card in html list
				var li = jQuery("<li></li>").addClass("list-group-item")
				.html("<i class='fas fa-user'></i> " + res.card.title).attr("data-id", res.card.id);

				jQuery(li).draggable({ 
					revert: "invalid",
					stack : "li"
				});

				jQuery(li).prependTo("#card-todo");
				jQuery("#success-alert").html(res.message).show();
				setTimeout(function(){
					$('#ModalTodoItem').modal('hide');
				},500);

			}else{
				// card already exist or failed.

			}


		});


	},

	ChangeCardType : function(li, newType){
		
		var data = {};

		data.id = jQuery(li).attr("data-id");
		data.type = newType;
		data.action = "ChangeCardType";

		this.ajax(data,function(res){



		});


	},

	sort : function(ul){

		var allLi = jQuery(ul).find("li");
		var liIndex = [];
		var data = {};

		//console.log(allLi);

		for(var i=0; i<= allLi.length - 1; ++i){
			liIndex.push(jQuery(allLi[i]).attr("data-id"));

		}

		data.action = "SortCardList";
		data.lis = liIndex;
		raptas.ajax(data,function(){

			// sort success
		})


	}


}


jQuery(document).ready(function() {

	
	// drag section
    jQuery( ".raptacards .card-body ul.list-group li" ).draggable({ 
	revert: "invalid",
	stack : "li"
	});
	
	// drop section
    jQuery( ".raptacards .card-body" ).droppable({
      drop: function( event, ui ) {
		 
        jQuery( this )
          .addClass( "ui-state-highlight" );
		 jQuery(ui.draggable).removeAttr("style");

		 var dropUl = jQuery(this).find("ul.list-group");
		 var dropUlId = jQuery(dropUl).attr("id");

		 if(dropUlId == "card-in-work"){
			raptas.ChangeCardType(ui.draggable, "in-work");
		 }else if(dropUlId == "card-done"){
			raptas.ChangeCardType(ui.draggable, "done");
		 }else if(dropUlId == "card-todo"){
			raptas.ChangeCardType(ui.draggable, "todo");
		 }

		 jQuery(ui.draggable).prependTo(jQuery(this).find("ul.list-group"));

		// sort the list
		raptas.sort(jQuery(this).find("ul.list-group"));
      }
	});
	






	// add todo section

	jQuery(document).on("click","#modal-save-btn", function(){	
		jQuery(".alert").hide();
		raptas.AddTodo();
	});
	

	
	
});